//
//  OrderCell.swift
//  Restaurant
//
//  Created by TISSA Technology on 11/4/20.
//

import UIKit

class OrderCell: UITableViewCell {

    @IBOutlet weak var ordernoLbl: UILabel!
    @IBOutlet weak var orderdateLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var paymentLbl: UILabel!
    @IBOutlet weak var detailShowBtn: UIButton!
    @IBOutlet weak var orderCancelBtn: UIButton!
    @IBOutlet weak var itemouterview: UIView!
    
    @IBOutlet weak var fullviewdetailsBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
