//
//  ingredientTVCellTableViewCell.swift
//  Restaurant
//
//  Created by TISSA Technology on 11/26/20.
//

import UIKit



class ingredientTVCellTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ingredientCellView: UIView!
    @IBOutlet weak var ingredientname: UILabel!
    @IBOutlet weak var ingredientPrice: UILabel!
    @IBOutlet weak var selctionimg: UIImageView!
    @IBOutlet weak var ingredientview: UIView!
    @IBOutlet weak var minusClicked: UIButton!
    @IBOutlet weak var plusClicked: UIButton!
    @IBOutlet weak var ingredientCountLBL: UILabel!
    @IBOutlet weak var selectwholeingBtn: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
